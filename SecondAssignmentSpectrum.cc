/*
Costruire una rete formata da almeno 4 nodi wireless: 2 Access Points  (AP) e 2 nodi (STA). 
- I due AP devono essere "vicini" (spazialmente) in modo da subire mutua interferenza. 
    Impostare i 2 AP in modo che usino 2 canali WiFi tali da generare mutua interferenza.
- Si associno i nodi STA a uno dei due AP, in modo che parte siano associati a un AP, e parte all'altro.
- Si connettano i due AP tramite una rete CSMA o P2P. In questo caso si provveda a mettere le necessarie regole di routing.
- Si generi traffico da o per i nodi STA (a piacere).

YansWiFiChannel
SpectrumWiFiChannel
*/

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ssid.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/nix-vector-routing-module.h"
#include "ns3/spectrum-analyzer-helper.h"
#include "ns3/spectrum-channel.h"
#include "ns3/spectrum-helper.h"
#include "ns3/spectrum-wifi-helper.h"
#include <chrono>


using namespace ns3;

NS_LOG_COMPONENT_DEFINE("SecondAssignmentSpectrum");

// Funzione di flow monitor
void CalculateThroughput (Ptr<FlowMonitor> monitor, FlowMonitorHelper &flowmon)
{
  // Creo una mappa stats dove la chiave è l'id del flusso e il valore è una struttura con le statistiche
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats();
  
  // classifier serve per ottenere informazioni sul flusso quali indirizzo ip sorgente e destinatario
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier());
  
  // Con un ciclo for scorro tutti i flussi 
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
    {
      Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
      std::cout << "Flow ID: " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
      std::cout << "  Tx Packets: " << i->second.txPackets << "\n";
      std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
      std::cout << "  Rx Packets: " << i->second.rxPackets << "\n";
      std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
      std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / (i->second.timeLastRxPacket.GetSeconds () - i->second.timeFirstTxPacket.GetSeconds ()) / 1024 / 1024 << " Mbps\n";
      std::cout << "  Packet Loss: " << i->second.lostPackets << "\n";
    }

  // Rischedulo il calcolo in modo che continui durante la simulazione
  Simulator::Schedule(Seconds(1.0), &CalculateThroughput, monitor, std::ref(flowmon));
}

int main (int argc, char *argv[])
{
    // Variabili di tipo uint32
    uint32_t nAP = 2;
    uint32_t nSTA = 2;
    uint32_t nSERVER = 1;

    // Permette di cambiare i parametri di simulazione senza dover ricompilare il codice
    CommandLine cmd(__FILE__);
    cmd.Parse (argc, argv);

    // Il livello di log da abilitare è quello informativo sull'esecuzione
    LogComponentEnable("SecondAssignmentSpectrum", LOG_LEVEL_INFO);

    // Creazione di 2 AP e 2 STA
    NodeContainer wifiApNodes;
    wifiApNodes.Create(nAP);
    NodeContainer wifiStaNodes;
    wifiStaNodes.Create(nSTA);
    NodeContainer server;
    server.Create(nSERVER); //nodo server per vedere l'interferenza

/////////////////////////////////////////////////////////////////////////////////////////////////

    // Impostazione dei canali WiFi con standard 80211n
    WifiHelper wifi;
    wifi.SetStandard(WIFI_STANDARD_80211n);

/////////////////////////////////////////////////////////////////////////////////////////////////

    SpectrumChannelHelper channelHelper = SpectrumChannelHelper::Default();
    channelHelper.SetChannel("ns3::MultiModelSpectrumChannel");
    channelHelper.AddSpectrumPropagationLoss("ns3::ConstantSpectrumPropagationLossModel");
    Ptr<SpectrumChannel> channel = channelHelper.Create();

    // Un solo canale fisico con Error Rate Model
    SpectrumWifiPhyHelper spectrumPhy;
    spectrumPhy.SetChannel(channel);
    spectrumPhy.SetErrorRateModel("ns3::NistErrorRateModel");

    // Canale per AP1 e STA1
    spectrumPhy.Set("ChannelSettings", StringValue("{1, 20, BAND_2_4GHZ, 0}")); 
    
    WifiMacHelper wifiMac1;
    Ssid ssid1 = Ssid("network-1");
    wifiMac1.SetType("ns3::StaWifiMac",
                      "Ssid", SsidValue(ssid1), 
                      "ActiveProbing", BooleanValue(false));
    NetDeviceContainer staDevices1 = wifi.Install(spectrumPhy, wifiMac1, wifiStaNodes.Get(0));
    wifiMac1.SetType("ns3::ApWifiMac",
                  "Ssid", SsidValue(ssid1));
    NetDeviceContainer apDevices1 = wifi.Install(spectrumPhy, wifiMac1, wifiApNodes.Get(0));

    // Canale per AP2 e STA2
    spectrumPhy.Set("ChannelSettings", StringValue("{2, 20, BAND_2_4GHZ, 0}")); // Simulo a 2 e 5
    
    WifiMacHelper wifiMac2;
    Ssid ssid2 = Ssid("network-2");
    wifiMac2.SetType("ns3::StaWifiMac",
                  "Ssid", SsidValue(ssid2),
                  "ActiveProbing", BooleanValue(false));
    NetDeviceContainer staDevices2 = wifi.Install(spectrumPhy, wifiMac2, wifiStaNodes.Get(1));
    wifiMac2.SetType("ns3::ApWifiMac",
                  "Ssid", SsidValue(ssid2));
    NetDeviceContainer apDevices2 = wifi.Install(spectrumPhy, wifiMac2, wifiApNodes.Get(1));

    // Connessione tra AP1, AP2 e Server tramite rete CSMA
    CsmaHelper csma;
    csma.SetChannelAttribute("DataRate", StringValue("100Mbps"));
    csma.SetChannelAttribute("Delay", TimeValue(NanoSeconds(6560)));
    NodeContainer csmaNetwork; 
    csmaNetwork.Add(server.Get(0));
    csmaNetwork.Add(wifiApNodes.Get(0));
    csmaNetwork.Add(wifiApNodes.Get(1));
    NetDeviceContainer csmaDevices = csma.Install(csmaNetwork);

/////////////////////////////////////////////////////////////////////////////////////////////////

    // Installo lo stack di Internet sui nodi e nix per il routing
    InternetStackHelper stack;
    Ipv4NixVectorHelper nix;
    stack.SetRoutingHelper(nix);
    stack.Install(wifiStaNodes);
    stack.Install(wifiApNodes);
    stack.Install(server);

/////////////////////////////////////////////////////////////////////////////////////////////////

    Ipv4AddressHelper address;

    // Assegnazione degli indirizzi IP agli STA
    address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer staInterfaces1 = address.Assign(staDevices1);
    Ipv4InterfaceContainer apInterfaces1 = address.Assign(apDevices1);

    address.SetBase("10.1.2.0", "255.255.255.0");
    Ipv4InterfaceContainer staInterfaces2 = address.Assign(staDevices2);
    Ipv4InterfaceContainer apInterfaces2 = address.Assign(apDevices2);

    // Assegnazione degli indirizzi IP per la connessione tra AP1 e AP2
    address.SetBase("10.1.3.0", "255.255.255.0");
    Ipv4InterfaceContainer csmaInterfaces = address.Assign(csmaDevices);

/////////////////////////////////////////////////////////////////////////////////////////////////

    // Configurazione della mobilità
    MobilityHelper mobility;
    mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                                    "MinX",
                                    DoubleValue(0.0),
                                    "MinY",
                                    DoubleValue(0.0),
                                    "DeltaX",
                                    DoubleValue(5.0),
                                    "DeltaY",
                                    DoubleValue(5.0),
                                    "GridWidth",
                                    UintegerValue(2),
                                    "LayoutType",
                                    StringValue("RowFirst"));
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(wifiStaNodes);
    mobility.Install(wifiApNodes);
    mobility.Install(server);

/////////////////////////////////////////////////////////////////////////////////////////////////

    // Configurazione del server Echo -> Server
    UdpEchoServerHelper echoServer(9);

    ApplicationContainer serverApps = echoServer.Install(server.Get(0));
    serverApps.Start(Seconds(1.0));
    serverApps.Stop(Seconds(20.0));

    // Configurazione del client Echo 1 -> STA1
    UdpEchoClientHelper echoClient(csmaInterfaces.GetAddress(2), 9);
    echoClient.SetAttribute("MaxPackets", UintegerValue(0));
    echoClient.SetAttribute("Interval", TimeValue(Seconds(0.001)));
    echoClient.SetAttribute("PacketSize", UintegerValue(4096));

    ApplicationContainer clientApps = echoClient.Install(wifiStaNodes.Get(0));
    clientApps.Start(Seconds(2.0)); // Inizia dopo 2 secondi per garantire che il server sia pronto
    clientApps.Stop(Seconds(18.0));

    // Configurazione del client Echo 2 -> STA2
    UdpEchoClientHelper echoClient2(csmaInterfaces.GetAddress(2), 9);
    echoClient2.SetAttribute("MaxPackets", UintegerValue(0));
    echoClient2.SetAttribute("Interval", TimeValue(Seconds(0.001)));
    echoClient2.SetAttribute("PacketSize", UintegerValue(4096));

    ApplicationContainer clientApps2 = echoClient2.Install(wifiStaNodes.Get(1));
    clientApps2.Start(Seconds(2.0)); // Inizia dopo 2 secondi per garantire che il server sia pronto
    clientApps2.Stop(Seconds(18.0));

/////////////////////////////////////////////////////////////////////////////////////////////////

    // Flow monitor
    FlowMonitorHelper flowmon;
    Ptr<FlowMonitor> monitor = flowmon.InstallAll();

    auto start = std::chrono::high_resolution_clock::now();
    Simulator::Stop(Seconds(22.0));
    Simulator::Run();
    auto end = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double> duration = end - start;
    std::cout << "Tempo di esecuzione della simulazione: " << duration.count() << " secondi\n";

   monitor->CheckForLostPackets();

    CalculateThroughput(monitor, flowmon);
    monitor->SerializeToXmlFile("flowmon-results.xml", true, true);

    Simulator::Destroy();

    return 0;
}



